/* WireCullFillShad.h */
#ifndef WIRECULLFILLSHAD_H
  #define WIRECULLFILLSHAD_H

# ifdef unix
  # include <curses.h>
  # define _getch getch
# else
  #include <conio.h> //wg. _getch()
# endif

  #include <math.h>
  #include <stdlib.h>
  #include "conDraw.h"
  #include "GrafOps.h" 
  #include "LoadCGF.h"
  #include "MatrOps.h" 
  #include "shadOps.h" 


  #define ESC        27
  #define CGF_FILE01 "..\\..\\_Dat\\Xface.cgf"
  #define CGF_FILE02            ".\\Xface.cgf"
  #define CGF_FILE11 "..\\..\\_Dat\\cube.cgf"
  #define CGF_FILE12            ".\\cube.cgf"
  #define CGF_FILE21 "..\\..\\_Dat\\house.cgf"
  #define CGF_FILE22            ".\\house.cgf"
  #define CGF_FILE31 "..\\..\\_Dat\\cylinder.cgf"
  #define CGF_FILE32            ".\\cylinder.cgf"

  #pragma warning(disable : 4996) // ("veraltet" nicht mehr melden!)

  /*Prototypen:*/
  float BBox          (CGFobject *obj);
  void  conBBox       (CGFobject *obj);
  int   conDrawCGFobj (CGFobject *obj, float eyez);
  void  conInit       (void);
  void  conViewport   (float *posMat);
  void  draw          (void);
  void  key           (char k);

#endif  //WIRECULLFILLSHAD_H
