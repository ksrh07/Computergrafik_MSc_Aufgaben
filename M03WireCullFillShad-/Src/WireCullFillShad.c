/* WireCullFillShad.c */
/*Darstellung dreidimensionaler Modelle im Konsole-Fenster*/
#define  MIT_LOADCGF
#define  MORE_WIRE

#include "WireCullFillShad.h"

/*Globale Variablen: */
CGFobject obj;
float     eyez=10., viewScale=1., angle[]={0.f, 0.f, 0.f};
float     BBoxCent[3];
int       backF=0, eyeClamp=0; 
int       wire=1;

/*Rotationswinkel Lichtquelle (ab Startposition):*/
float     lightAng[2]={.0f, .0f}; 

/*************************************************************************/
  void conViewport (float *posMat)
/*************************************************************************/
/*Viewport-Trf. zur Konsolen-Darstellung*/ 
{ float xCenter=(CON_XMAX + CON_XMIN)/2, yCenter=(CON_YMAX + CON_YMIN)/2;

  /*Skalierung zur Bildfuellung und zur Korrektur der Schrift 8x12
    (viewScale u. zusaetzlicher Faktor 3/2 in x-Richtung)*/
#ifdef   MORE_WIRE
#endif //MORE_WIRE

  /*Translation ans Konsole-Zentrum:*/
  translate (xCenter, yCenter, 0, posMat);
  return;
}

/*************************************************************************/
  int conDrawCGFobj (CGFobject *obj, float eyez)
/*************************************************************************/
/*CGF-Objekt auf Konsole zeichnen:*/
{ int   jj1=0, jj2=0, hidden=0, line0[2], lineN[2];
  char  symbol=0;
  float trfPnt0[NDIM], trfPntN[NDIM], trfPntChk[NDIM], //Dummies fuer 3 Punkte
        dummyPosMat[NDIM*NDIM], dummyEye[]={0.f, 0.f, 0.f, 1.f};
  float light[NDIM]={-1.f, 1.f, 1.f, 1.f};//Ortsvektor Lichtquelle
  float normal0[NDIM], dummyf=0.f; 

  /*Positionierung der Lichtquelle:*/
  turnRndXY (light, lightAng);

  dummyEye[Z]=eyez;

  /*Bisherige Bewegungen vergessen, Objekt um Koord.-Ursprung positionieren:*/
  Identity (obj->posMat, NDIM);
  translate (-BBoxCent[X], -BBoxCent[Y], -BBoxCent[Z], obj->posMat);

  /*Bisherige Bewegung neu aufbauen (ohne Fehlerfortpflanzung):*/
  rotate (angle[X], X, obj->posMat);
  rotate (angle[Y], Y, obj->posMat);
  rotate (angle[Z], Z, obj->posMat);
  /*3D-Pos.Matrix sichern (fuer BackFculling):*/
  saveMatAs (obj->posMat, dummyPosMat, NDIM*NDIM);

  /*Persp. Trf. zur aktuellen Pos.Matrix:*/
  perspTrf_z0 (eyez, obj->posMat);
  /*Viewport-Trf. zur Konsolen-Darstellung:*/
  conViewport (obj->posMat);

  /*Modell flaechenweise zeichnen:*/
  for (jj1=0; jj1<obj->nFace; jj1++) 
  { /*(Falls gefordert) Sichtbarkeit ueber die ersten 3 Pkte pruefen:*/
#ifdef  MORE_WIRE
#endif//MORE_WIRE

    if (backF && !hidden && !wire)
    { initF4F (endL, endR);
      if (shade == 's') // Flat shading: 1x pro Flaeche
      { normalF (trfPnt0, trfPntN, trfPntChk, normal0); //Flaechennormale
        normV (light);  normV (normal0); //Normierung Licht, Normale
        dummyf = reflex (light, normal0, .1f); //Anteil Lichtreflexion
        symbol = grey[gLevel(dummyf, G_LEVELS)]; //Zuordnung Helligkeit-ASCII
      }
    }

    /*Flaechenpunkte miteinander verbinden:*/
    for (jj2=0; jj2<obj->Face[jj1].nPnt; jj2++) 
    { if (backF && hidden) break;
#ifdef  MORE_WIRE
#endif//MORE_WIRE
    }

    /*Nun Flaeche fuellen:*/
    if (backF && !hidden && !wire) 
    {
      fillF   (endL, endR, symbol);
    } /*Aktuelle Flaeche fertig*/
  } /*Alle Flaechen fertig*/
  return 1;
}

/*************************************************************************/
  void conBBox(CGFobject *obj)
/*************************************************************************/
/*Ermittlung der Bounding Box (parallel zu den Hauptachsen):*/
{ int   jj1=0, jj2=0;
  float BBoxMin[3], BBoxMax[3], d=0.;

  /*Bounding Box:*/
#ifdef   MORE_WIRE
#endif //MORE_WIRE

  /*Ermittlung des BBox-Zentrums und des bildfuellenden Massstabs;
    (max.)Raumdiagonale BBox soll auf (min.)CON_YMAX skaliert werden:*/
  viewScale=d=0.;
  for (jj1=0; jj1<3; jj1++) 
  { BBoxCent[jj1] = (BBoxMax[jj1] + BBoxMin[jj1])/2;
    d += (BBoxMax[jj1]-BBoxMin[jj1])*(BBoxMax[jj1]-BBoxMin[jj1]);
  } d = (float)pow(d, .5);
  viewScale = (CON_YMAX - CON_YMIN) / d;

  return;
}

/*************************************************************************/
  void conInit(void)
/*************************************************************************/
/*(Erst- & einmalige Programm-Initialisierung inkl. Laden eines Objektes:*/
{ system (CON_SIZE); // Fenster "auf Groesse" bringen
  conClrBuff();      // Speicher mit Leerzeichen "loeschen"
  conClrScr();       // Konsole-Fenster loeschen

  /*G_LEVELS Grautoene in abnehmendem Kontrast zum Hintergrund:*/
  //grey[G_LEVELS-5]= BLACK; /*'\040'*/  // Hintergrundfarbe
  grey[G_LEVELS-4]= DGREY;/*'\260'*/ grey[G_LEVELS-3]= MGREY;/*'\261'*/
  grey[G_LEVELS-2]= LGREY;/*'\262'*/ grey[G_LEVELS-1]= WHITE;/*'\333'*/

#ifdef   MIT_LOADCGF
  if (!loadCGF (&obj, CGF_FILE11) && !loadCGF (&obj, CGF_FILE12) && 
      !loadCGF (&obj, CGF_FILE21) && !loadCGF (&obj, CGF_FILE22))
#endif //MIT_LOADCGF
  setTriangleCGF (&obj);
  Identity (obj.posMat, NDIM);
  printCGF (&obj);   // Werte geladener Struktur-Variablen ausgeben
  conBBox (&obj);    // Bounding Box ermitteln
  return;
}

/*************************************************************************/
  void initPos(CGFobject *obj)
/*************************************************************************/
/*Erstmalige Positionierung und BBox-Berechnung eines geladenen Objekts:*/
{ printCGF (obj); backF = 0; eyez = 10.f; 
  wire=1; 
  shade=0;
  lightAng[X] = lightAng[Y] = 0.f;
  Identity (obj->posMat, NDIM);
  angle[X] = angle[Y] = angle[Z] =0.; 
  conBBox(obj);
  return;
}

/*************************************************************************/
  void draw (void)
/*************************************************************************/
/*Standard-Struktur eines Grafik-Programms:*/
{ conClrBuff();              //Speicher loeschen
  conDrawCGFobj(&obj, eyez); //Grafik im Hintergrund aktualisieren+ausgeben
  conSwapBuff();             //Hintergrund-Puffer sichtbar machen
  if (eyeClamp) printf ("\rEye point clamped (no clipping available)");

  return;
}

/*************************************************************************/
  void key (char k)
/*************************************************************************/
/*Menue und Eingabe-Behandlung:*/
{ switch (k)
  { 
#ifdef MIT_LOADCGF
    case '0': if (loadCGF (&obj, CGF_FILE01) || loadCGF (&obj, CGF_FILE02))
                initPos(&obj);
              else { conClrScr(); printf ("\n\n\r  LOAD FAILED !"); _getch(); }
              break;
    case '1': if (loadCGF (&obj, CGF_FILE11) || loadCGF (&obj, CGF_FILE12))
                initPos(&obj);
              else { conClrScr(); printf ("\n\n\r  LOAD FAILED !"); _getch(); }
              break;
    case '2': if (loadCGF (&obj, CGF_FILE21) || loadCGF (&obj, CGF_FILE22))
                initPos(&obj);
              else { conClrScr(); printf ("\n\n\r  LOAD FAILED !"); _getch(); }
              break;
    case '4': if (loadCGF (&obj, CGF_FILE31) || loadCGF (&obj, CGF_FILE32))
                initPos(&obj);
              else { conClrScr(); printf ("\n\n\r  LOAD FAILED !"); _getch(); }
              break;
#endif //MIT_LOADCGF
    case '3': if (setTriangleCGF (&obj)) 
                initPos(&obj);
              else { conClrScr(); printf ("\n\n\r  LOAD FAILED !"); _getch(); }
              break;
    case 'b': backF = 1-backF; 
              if (!wire) backF = 1; 
                                                    break;
    case 'e': if (!eyeClamp) eyez *=.9f;            break;
    case 'E': eyez *=1.1f;  eyeClamp = 0;           break;
    case 'l': if (!shade)                           break;
              lightAng[Y] += 30.; 
              if (lightAng[Y] >= 360) lightAng[Y]-=360; break;
    case 'L': if (!shade)                           break;
              lightAng[X] += 30.; 
              if (lightAng[X] >= 360) lightAng[X]-=360; break;
    case 's': if (wire)                             break;
              if (!shade) { shade='s'; backF = 1; }
              else shade=0;  wire = 0; 
                                                    break;
    case 'w': wire = 1-wire; 
              shade=0; 
              if (!wire) backF = 1;                 break;
    case 'x': angle[X] -= 5.; if (angle[X] <=-360) angle[X]+=360; break;
    case 'X': angle[X] += 5.; if (angle[X] >= 360) angle[X]-=360; break;
    case 'y': angle[Y] -= 5.; if (angle[Y] <=-360) angle[Y]+=360; break;
    case 'Y': angle[Y] += 5.; if (angle[Y] >= 360) angle[Y]-=360; break;
    case 'z': angle[Z] -= 5.; if (angle[Z] <=-360) angle[Z]+=360; break;
    case 'Z': angle[Z] += 5.; if (angle[Z] >= 360) angle[Z]-=360; break;
  }
  if (k == 'h')
  { conClrScr(); 
    printf ("\n\r Press:");
    printf ("\n\r <ESC> to quit");
#ifdef   MIT_LOADCGF
    printf ("\n\r 0...4    load Xface/cube/house/triang./cylinder");
#endif //MIT_LOADCGF
    printf ("\n\r b        toggle Backface culling ");
    if (backF) printf ("(ON)"); else printf ("(OFF)"); 
    printf ("\n\r e / E    decrease/increase Eye point z-coord.");
    printf ("\n\r h        Help (this menu)");
    printf ("\n\r L / l    turn light around X/Y (curr.:%d/%d)", 
                            (int)lightAng[X], (int)lightAng[Y]);
    printf ("\n\r s        toggle Shading Flat"); 
    if (shade=='s') printf ("(ON)"); else printf ("(OFF)");
    printf ("\n\r w        toggle Wire frame "); if ( wire) printf ("(ON)"); 
    printf ("/ face filling ");                     if (!wire) printf ("(ON)"); 
    printf ("\n\n\r x / X    decrease/increase X-rotation angle");
    printf ("\n\r y / Y    decrease/increase Y-rotation angle");
    printf ("\n\r z / Z    decrease/increase Z-rotation angle");
    printf ("\n\n\r Current values: eyez    =%7.2f", eyez);
    if (eyeClamp) printf (" (clamped)");
    printf ("\n\r                 angle[X]=%7.2f", angle[X]);
    printf ("\n\r                 angle[Y]=%7.2f", angle[Y]);
    printf ("\n\r                 angle[Z]=%7.2f", angle[Z]);
    printf ("\n\n\r(press any key to continue)"); 
    _getch(); draw();
  }
  return;
}

#define  WIRECULLFILLSHAD_WITH_MAIN
#ifdef   WIRECULLFILLSHAD_WITH_MAIN
/*************************************************************************/
  int main()
/*************************************************************************/
{ int keypress=0;
  system (CON_SIZE);
  printf ("\n\n\r  Aris Christidis: \
           \n\n\r  Load & Render CGF Object on Console \
           \n\n\r  [ help: <h>, quit: <ESC> ]\
           \n\n\r  (press any key)"); _getch();
  conInit();
  while (keypress != ESC)
  { draw();
    keypress = _getch();
    key ((char)keypress);
  }
  return 0;
}
#endif //WIRECULLFILLSHAD_WITH_MAIN
