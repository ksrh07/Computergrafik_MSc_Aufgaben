/* InterpoLine.h */
#ifndef INTERPOLINE_H
  #define INTERPOLINE_H

# ifdef unix
  # include <curses.h>
  # define _getch getch
# else
  #include <conio.h> //wg. _getch()
# endif

  #include "conDraw.h"

  #define ESC     27
  #define CR      13

  /*max. Anzahl Pixel einer Kante (Schattierung, Texturierung):*/
  #define LINLEN MAX(CON_YMAX-CON_YMIN,CON_XMAX-CON_XMIN)+1 
  /*max. Anzahl angezeigter Zeilen einer Objektflaeche:*/
  #define FACE_Y  (CON_YMAX - CON_YMIN+1)

  #pragma warning(disable : 4996) // ("veraltet" nicht mehr melden!)

  /*Prototypen:*/
  void conInit (void);
  void draw    (void);
  void key     (char k);
#endif  //INTERPOLINE_H
