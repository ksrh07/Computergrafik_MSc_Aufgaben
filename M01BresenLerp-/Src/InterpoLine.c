/* InterpoLine.c */
/* Visualisierung einer ganzzahligen linearen Interpolation, gestuetzt 
   auf den Bresenham-Algorithmus, mit Visualisierung im Konsole-Fenster. 
   A. Christidis */
# ifdef unix
  # define printf printw
# endif
#include "interOps.h"
#include "InterpoLine.h"

/*Globale Variablen: */
int  xLin[]={0,1}, yLin[]={0,0}, xOffset=CON_XMIN, yOffset=CON_YMAX/2, 
     yStore[], CurPnt=1, axes=1;
int  yStore[LINLEN];
//char symboLin='\333'; //alternativ: ='\376';
char symboLin='X';

/*************************************************************************/
  void draw (void)
/*************************************************************************/
/*Standard-Struktur eines Grafik-Programms:*/
{ int j1=0,sign=0;
  /*Vorbereitungen:*/
  for (j1=0; j1 < LINLEN; j1++) yStore[j1]=-99;
  conClrBuff();      //Speicher loeschen

  /*Achsenkreuz:*/
  if (axes)
  { for (j1=0; j1 < CON_XMAX; j1++) conSetPixel (j1, yOffset, '-');
    for (j1=0; j1 < CON_YMAX; j1++) conSetPixel (xOffset, j1, '|');
    conSetPixel (CON_XMAX, yOffset, '>');
    conSetPixel (xOffset, CON_YMAX, '^');
    conSetPixel (xOffset, yOffset, '+');
  }

  /*Linie nach Bresenham:*/
  //MidpointLine (xLin[0]+xOffset, yLin[0]+yOffset, xLin[1]+xOffset, yLin[1]+yOffset, '\333');
  //conSetPixel (xLin[1]+xOffset, yLin[1]+yOffset, '\333'); //Endpunkt

  /*Grafik im Hintergrund aktualisieren+ausgeben:*/
  set_iLerp (abs(xLin[1]-xLin[0])+1, yLin[0], yLin[1], yStore); 
  if (xLin[0] < xLin[1]) sign=1; else sign=-1; 
  for (j1=xLin[0]; j1 != xLin[1]; j1+=sign) 
    conSetPixel (j1+xOffset, yStore[j1]+yOffset, symboLin);
    conSetPixel (xLin[1]+xOffset, yStore[j1]+yOffset, symboLin);

  conSwapBuff();     //Hintergrund-Puffer sichtbar machen
  printf ("\r[%d,%d]---[%d,%d] \t <<type 'h' for help>>\r", 
         xLin[0], yLin[0], xLin[1], yLin[1]);
  return;
}

/*************************************************************************/
  void conInit(void)
/*************************************************************************/
/*Erst- & einmalige Programm-Initialisierung:*/
{
# ifdef unix
# else
  system (CON_SIZE); // Fenster "auf Groesse" bringen
# endif
  conClrBuff();      // Speicher mit Leerzeichen "loeschen"
  conClrScr();       // Konsole-Fenster loeschen
  return;
}

/*************************************************************************/
  void key (char k)
/*************************************************************************/
/*Menue und Eingabe-Behandlung:*/
{ int jj;
  switch (k)
  { case CR : CurPnt = 1-CurPnt; 
              break;
    case ESC: if (!xLin[0] && !xLin[1] && !yLin[0] && !yLin[1]) exit(0);
              else {xLin[0]=0; xLin[1]=0; yLin[0]=0; yLin[1]=0; CurPnt=1;}
              break;
    case '+': axes = 1-axes;
              break;
    case 'd': if (CurPnt==1 && xLin[CurPnt] < CON_XMAX-xOffset) 
              xLin[CurPnt]++;
              break;
    case 'w': if (yLin[CurPnt] < CON_YMAX-yOffset) yLin[CurPnt]++;
              break;
    case 'a': if (CurPnt==1 && xLin[CurPnt] > -xOffset)  xLin[CurPnt]--; 
              break;
    case 's': if (yLin[CurPnt] > -yOffset)  yLin[CurPnt]--;  break;
  }
  if (k == 'h')
  { conClrScr(); 
    printf ("\n\r Press:");
    printf ("\n\n\r <ESC>  to purge / quit");
    printf ("\n\n\r <CR>      toggle start"); if (!CurPnt) printf ("(ON)");
    printf ("/end"); if (CurPnt) printf ("(ON)"); printf (" point of line"); 

    printf ("\n\n\r +         toggle show"); if (axes) printf ("(ON)");
    printf ("/hide"); if (!axes) printf ("(ON)"); printf (" coord. axes"); 
    printf ("\n\n\r a / d     decrease/increase point x-coord.");
    printf ("\n\n\r s / w     decrease/increase point y-coord.");
    printf ("\n\n\r h         Help (this menu)");
    printf ("\n\n\r Current end points: [%d,%d]---[%d,%d]", 
            xLin[0], yLin[0], xLin[1], yLin[1]);
    printf ("\n\n\r yield: ["); 
    for (jj=0; jj<xLin[1]-xLin[0]+1; jj++) printf (" %d", yStore[jj]);
    printf ("]"); 
    if (xLin[1]!=xLin[0]) 
    { printf ("\n\n\r accurately: ["); 
      for (jj=0; jj<xLin[1]-xLin[0]+1; jj++) printf (" %.2f", 
               yLin[0] + jj*(float)(yLin[1]-yLin[0])/(xLin[1]-xLin[0]));
      printf ("]"); 
    }
    printf ("\n\n\r(press any key to continue)"); _getch();
    draw();
  }
  return;
}

# ifdef unix
void cleanup()
{
  endwin();
}
# endif

/*************************************************************************/
  int main()
/*************************************************************************/
{ int keypress=' ';
# ifdef unix
  initscr();
  atexit(cleanup);
# else
  system (CON_SIZE);
# endif
  printf ("\n\n\r  Aris Christidis: \
           \n\n\r  Bresenham Based Interpolation Algorithm\
           \n\n\r  [ help: <h>, quit: <ESC> ]\
           \n\n\r  (press any key)"); getch();
  conInit();
  while (keypress)
  { draw();
    keypress = getch();
    key ((char)keypress);
  }
  return 0;
}
