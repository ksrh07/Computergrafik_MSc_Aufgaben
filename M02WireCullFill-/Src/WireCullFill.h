/* WireCullFill01.h */
#ifndef WIRECULLFILL_H
  #define WIRECULLFILL_H

# ifdef unix
  # include <curses.h>
  # define _getch getch
# else
  #include <conio.h> //wg. _getch()
# endif

  #include <math.h>
  #include <stdlib.h>
  #include "conDraw.h"
  #include "fillOps.h" 
  #include "GrafOps.h" 
  #include "lineOps.h" 
  #include "LoadCGF.h"
  #include "MatrOps.h" 

  #define ESC        27
  #define CGF_FILE01 "..\\..\\_Dat\\Xface.cgf"
  #define CGF_FILE02            ".\\Xface.cgf"
  #define CGF_FILE11 "..\\..\\_Dat\\cube.cgf"
  #define CGF_FILE12            ".\\cube.cgf"
  #define CGF_FILE21 "..\\..\\_Dat\\house.cgf"
  #define CGF_FILE22            ".\\house.cgf"

  #pragma warning(disable : 4996) // ("veraltet" nicht mehr melden!)

  /*Prototypen:*/
  float BBox          (CGFobject *obj);
  void  conBBox       (CGFobject *obj);
  int   conDrawCGFobj (CGFobject *obj, float eyez);
  void  conInit       (void);
  void  conViewport   (float *posMat);
  void  draw          (void);
  void  key           (char k);

#endif  //WIRECULLFILL_H
