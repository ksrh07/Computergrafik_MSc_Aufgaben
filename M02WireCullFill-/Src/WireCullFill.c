/* WireCullFill.c */
/*Darstellung dreidimensionaler Modelle im Konsole-Fenster*/
#define  MIT_LOADCGF
#define  MORE_WIRE

#include "WireCullFill.h"

/*Globale Variablen: */
CGFobject obj;
float     eyez=10., viewScale=1., angle[]={0., 0., 0.};
float     BBoxCent[3];
int       backF=0; 
int       wire=1; 

/*************************************************************************/
  void conViewport (float *posMat)
/*************************************************************************/
/*Viewport-Trf. zur Konsolen-Darstellung*/ 
{ float xCenter=(CON_XMAX + CON_XMIN)/2, yCenter=(CON_YMAX + CON_YMIN)/2;

  /*Skalierung zur Bildfuellung und zur Korrektur der Schrift 8x12
    (viewScale u. zusaetzlicher Faktor 3/2 in x-Richtung)*/
#ifdef   MORE_WIRE
#endif //MORE_WIRE

  /*Translation ans Konsole-Zentrum:*/
  translate (xCenter, yCenter, 0, posMat);

  return;
}

/*************************************************************************/
  int conDrawCGFobj (CGFobject *obj, float eyez)
/*************************************************************************/
/*CGF-Objekt auf Konsole zeichnen:*/
{ int   jj1=0, jj2=0, jj3=0, hidden=0;
  char  symbol=0;
  float dummy0[4], dummyN[4], dummyChk[4], //Dummies fuer 3 Punkte
        dummyPosMat[4*4], dummyEye[]={0., 0., 0., 1.};
  dummyEye[Z]=eyez;

  /*Bisherige Bewegungen vergessen u. um Koord.-Ursprung positionieren:*/
  Identity (obj->posMat, 4);
  translate (-BBoxCent[X], -BBoxCent[Y], -BBoxCent[Z], obj->posMat);

  /*Bisherige Bewegung neu aufbauen (ohne Fehlerfortpflanzung):*/
  rotate (angle[X], X, obj->posMat);
  rotate (angle[Y], Y, obj->posMat);
  rotate (angle[Z], Z, obj->posMat);
  /*3D-Pos.Matrix sichern (fuer BackFculling):*/
  for (jj1=0; jj1<4*4; jj1++) dummyPosMat[jj1] = obj->posMat[jj1];

  /*Persp. Trf. zur aktuellen Pos.Matrix:*/
  perspTrf_z0 (eyez, obj->posMat);
  /*Viewport-Trf. zur Konsolen-Darstellung:*/
  conViewport (obj->posMat);

  /*Modell flaechenweise zeichnen:*/
  for (jj1=0; jj1<obj->nFace; jj1++) 
  { /*(Falls gefordert) Sichtbarkeit ueber die ersten 3 Pkte pruefen:*/
#ifdef   MORE_WIRE
#endif //MORE_WIRE
    /*Flaechenpunkte miteinander verbinden:*/
    for (jj2=0; jj2<obj->Face[jj1].nPnt; jj2++) 
    { 
      if (backF && hidden) break;
#ifdef   MORE_WIRE
#endif //MORE_WIRE
    }
    /*Nun Flaeche fuellen:*/
    if (backF && !hidden && !wire) 
    {
      fillF   (endL, endR, obj->Face[jj1].symbol);
    }
  }
  return 1;
}

/*************************************************************************/
  void conBBox(CGFobject *obj)
/*************************************************************************/
/*Ermittlung der Bounding Box (parallel zu den Hauptachsen):*/
{ int   jj1=0, jj2=0;
  float BBoxMin[3], BBoxMax[3], d=0.;

  /*Bounding Box:*/
#ifdef   MORE_WIRE
#endif //MORE_WIRE

  /*Ermittlung des BBox-Zentrums und des bildfuellenden Massstabs;
    (max.)Raumdiagonale BBox soll auf (min.)CON_YMAX skaliert werden:*/
  viewScale=d=0.;
  for (jj1=0; jj1<3; jj1++) 
  { BBoxCent[jj1] = (BBoxMax[jj1] + BBoxMin[jj1])/2;
    d += (BBoxMax[jj1]-BBoxMin[jj1])*(BBoxMax[jj1]-BBoxMin[jj1]);
  } d = (float)pow(d, .5);
  viewScale = (CON_YMAX - CON_YMIN) / d;

  return;
}

/*************************************************************************/
  void conInit(void)
/*************************************************************************/
/*(Erst- & einmalige Programm-Initialisierung inkl. Laden eines Objektes:*/
{ system (CON_SIZE); // Fenster "auf Groesse" bringen
  conClrBuff();      // Speicher mit Leerzeichen "loeschen"
  conClrScr();       // Konsole-Fenster loeschen

#ifdef   MIT_LOADCGF
  if (!loadCGF (&obj, CGF_FILE11) && !loadCGF (&obj, CGF_FILE12) && 
      !loadCGF (&obj, CGF_FILE21) && !loadCGF (&obj, CGF_FILE22))
#endif //MIT_LOADCGF
  setTriangleCGF (&obj);
  Identity (obj.posMat, 4);
  printCGF (&obj);   // Werte geladener Struktur-Variablen ausgeben
  conBBox (&obj);    // Bounding Box ermitteln
  return;
}

/*************************************************************************/
  void initPos(CGFobject *obj)
/*************************************************************************/
/*Erstmalige Positionierung und BBox-Berechnung eines geladenen Objekts:*/
{ printCGF (obj); backF = 0; eyez = 10.f; 
  wire=1; 
  Identity (obj->posMat, 4);
  angle[X] = angle[Y] = angle[Z] =0.; 
  conBBox(obj);
  return;
}

/*************************************************************************/
  void draw (void)
/*************************************************************************/
/*Standard-Struktur eines Grafik-Programms:*/
{ conClrBuff();              //Speicher loeschen
  conDrawCGFobj(&obj, eyez); //Grafik im Hintergrund aktualisieren+ausgeben
  conSwapBuff();             //Hintergrund-Puffer sichtbar machen

  return;
}

/*************************************************************************/
  void key (char k)
/*************************************************************************/
/*Menue und Eingabe-Behandlung:*/
{ switch (k)
  { case ESC: exit(0);
#ifdef MIT_LOADCGF
    case '0': if (loadCGF (&obj, CGF_FILE01) || loadCGF (&obj, CGF_FILE02))
                initPos(&obj);
              else { conClrScr(); printf ("\n\n\r  LOAD FAILED !"); _getch(); }
              break;
    case '1': if (loadCGF (&obj, CGF_FILE11) || loadCGF (&obj, CGF_FILE12))
                initPos(&obj);
              else { conClrScr(); printf ("\n\n\r  LOAD FAILED !"); _getch(); }
              break;
    case '2': if (loadCGF (&obj, CGF_FILE21) || loadCGF (&obj, CGF_FILE22))
                initPos(&obj);
              else { conClrScr(); printf ("\n\n\r  LOAD FAILED !"); _getch(); }
              break;
#endif //MIT_LOADCGF
    case '3': if (setTriangleCGF (&obj)) 
                initPos(&obj);
              else { conClrScr(); printf ("\n\n\r  LOAD FAILED !"); _getch(); }
              break;
    case 'b': backF = 1-backF; 
              if (!wire) backF = 1; 
              break;
    case 'e': eyez *=.9f;                           break;
    case 'E': eyez *=1.1f;                          break;
    case 'w': wire = 1-wire;  if (!wire) backF = 1; break;
    case 'x': angle[X] -= 5.; if (angle[X] <=-360) angle[X]+=360; break;
    case 'X': angle[X] += 5.; if (angle[X] >= 360) angle[X]-=360; break;
    case 'y': angle[Y] -= 5.; if (angle[Y] <=-360) angle[Y]+=360; break;
    case 'Y': angle[Y] += 5.; if (angle[Y] >= 360) angle[Y]-=360; break;
    case 'z': angle[Z] -= 5.; if (angle[Z] <=-360) angle[Z]+=360; break;
    case 'Z': angle[Z] += 5.; if (angle[Z] >= 360) angle[Z]-=360; break;
  }
  if (k == 'h')
  { conClrScr(); 
    printf ("\n\r Press:");
    printf ("\n\n\r <ESC>  to quit");
#ifdef   MIT_LOADCGF
    printf ("\n\n\r 0/1/2/3   load+render Xface/cube/house/triangle");
#endif //MIT_LOADCGF
    printf ("\n\n\r b         toggle Backface culling ");
    if (backF) printf ("(ON)"); else printf ("(OFF)"); 
    printf ("\n\n\r e / E     decrease/increase Eye point z-coord.");
    printf ("\n\n\r h         Help (this menu)");
    printf ("\n\n\r w         toggle Wire frame "); if ( wire) printf ("(ON)"); 
    printf ("/ face filling ");                     if (!wire) printf ("(ON)"); 
    printf ("\n\n\r x / X     decrease/increase X-rotation angle");
    printf ("\n\r y / Y     decrease/increase Y-rotation angle");
    printf ("\n\r z / Z     decrease/increase Z-rotation angle");
    printf ("\n\n\r Current values: eyez    =%7.2f", eyez);
    printf ("\n\r                 angle[X]=%7.2f", angle[X]);
    printf ("\n\r                 angle[Y]=%7.2f", angle[Y]);
    printf ("\n\r                 angle[Z]=%7.2f", angle[Z]);
    printf ("\n\n\r(press any key to continue)"); 
    _getch(); draw();
  }
  return;
}

#define  WIRECULL_WITH_MAIN
#ifdef   WIRECULL_WITH_MAIN
/*************************************************************************/
  int main()
/*************************************************************************/
{ int keypress=0;
  system (CON_SIZE);
  printf ("\n\n\r  Aris Christidis: \
           \n\n\r  Load & Render CGF Object on Console \
           \n\n\r  [ help: <h>, quit: <ESC> ]\
           \n\n\r  (press any key)"); _getch();
  conInit();
  while (keypress != ESC)
  { draw();
    keypress = _getch();
    key ((char)keypress);
  }
  return 0;
}
#endif //WIRECULL_WITH_MAIN

