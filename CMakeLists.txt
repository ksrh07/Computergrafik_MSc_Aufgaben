cmake_minimum_required( VERSION 2.8)
project(Computergrafik_Aufgaben)

FILE(GLOB C_SRCS _Libs/*.c)
include_directories(_Libs)

add_subdirectory(M01BresenLerp-)
add_subdirectory(M02WireCullFill-)
#add_subdirectory(M03WireCullFillShad-)
#add_subdirectory(M0456ObjElabGLaaTx-)
