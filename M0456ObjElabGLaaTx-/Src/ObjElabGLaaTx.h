/* ObjElabGLaaTx.h */
#ifndef OBJELAB_H
  #define OBJELAB_H

  #define CGF_FILE01 "..\\..\\_Dat\\Xface.cgf"
  #define CGF_FILE02            ".\\Xface.cgf"
  #define CGF_FILE11 "..\\..\\_Dat\\cube.cgf"
  #define CGF_FILE12            ".\\cube.cgf"
  #define CGF_FILE21 "..\\..\\_Dat\\house.cgf"
  #define CGF_FILE22            ".\\house.cgf"
  #define CGF_FILE41 "..\\..\\_Dat\\cylinder.cgf"
  #define CGF_FILE42            ".\\cylinder.cgf"

  #pragma warning(disable : 4996) // ("veraltet" nicht mehr melden!)

  /*Prototypen:*/
  void display    (void);
  void drawObj    (CGFobject *obj);
  void init       (void);
  void initPos    (CGFobject *obj);
  void key        (unsigned char key, int x, int y);
  void move       (void);

#ifdef  WITH_TEXR
  int  checkSize (int x);
  void TexInit   (void);
#endif//WITH_TEXR

#ifdef  WITH_AA
  void drawAAObj  (CGFobject *obj);
#endif//WITH_AA

#ifdef  WITH_LITE
#ifdef  DBG_LITE
  void checkMe    (void);
#endif//DBG_LITE
  void LitInit    (void);
  void rotLitY    (float *posit, float yAngle);
#endif//WITH_LITE

#endif //OBJELAB_H
