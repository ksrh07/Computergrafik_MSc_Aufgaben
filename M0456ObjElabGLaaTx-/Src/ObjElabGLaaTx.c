/* ObjElabGLaaTx.c */
/*Darstellung eines dreidimensionalen CGF-Objekts mit OpenGL*/

//#define DBL_BUF
#define WITH_AA
#define MORE_AA

#define WITH_TEXR
#define MORE_TEXR

#define WITH_LITE
#define MORE_LITE

#define MORE_OBJ
#define MIT_LOADCGF

#define CON_CLS   "cls"
#define ESC        27
#ifdef  WITH_TEXR
  #define RGB       3
  #define SPC      32
#endif//WITH_TEXR
#ifdef  MORE_TEXR
//#else //MORE_TEXR
  #define LFT        0.
  #define BTM        0.
  #define RIT        1.
  #define TOP        1.
#endif//MORE_TEXR

#ifndef MIN
  #define MIN(x,y) (( (x) < (y) ) ? (x) : (y))
#endif  MIN
#ifndef MAX  
  #define MAX(x,y) (( (x) > (y) ) ? (x) : (y))
#endif  MAX

# ifdef unix
  # include <curses.h>
  # define _getch getch
# else
  #include <conio.h> //wg. _getch()
# endif
#include <math.h>   //wg. pow()
#include <stdio.h>  //wg. printf()
#include <stdlib.h> //wg. system()
#include <GL/glut.h>//wg. Konflikte: glut.h nach stdlib.h
#include "auxOps.h"
#include "LoadCGF.h"
#include "ObjElabGLaaTx.h"
#ifdef  WITH_TEXR
#include "diceTex.h" 
#endif//WITH_TEXR
#ifdef  WITH_AA
#include "jitter.h"

/*Globale Variablen: */
float   aaOff=1.;
int     aaJit[]={2, 3, 4, 8, 15, 24, 66}, aaIdx=3, aaMod=1;
#endif//WITH_AA

#ifdef  WITH_TEXR
GLuint  texName[6];
int     texOn=0, texFil=GL_LINEAR;
#endif//WITH_TEXR

CGFobject obj;
float   eyez=10., diag=10., viewScale=1., angle[3]={0.,0.,0.};
int     backF=0, persp=1, winWidth=300, winHeight=300; 
int     aa=0, shade=GL_FLAT, stipple=0, wire=GL_LINE;

#ifdef  WITH_LITE
int     LitOn=0;
GLfloat LitAngY=0., LitPos0[] = {-1.0, 1.0, 1.0, 0.0}; 
#endif//WITH_LITE

GLdouble  nah=0., fern=0.;
GLfloat   colors[][4] = {
{1.0, 1.0, 1.0, 1.0},/*white  */
{1.0, 0.0, 0.0, 1.0},/*red    */
{0.0, 1.0, 0.0, 1.0},/*green  */
{0.0, 0.0, 1.0, 1.0},/*blue   */
{1.0, 1.0, 0.0, 1.0},/*yellow */
{1.0, 0.0, 1.0, 1.0},/*magenta*/
{0.0, 1.0, 1.0, 1.0},/*cyan   */
{0.6, 0.6, 0.6, 1.0},/*bright */
{0.3, 0.3, 0.3, 1.0},/*dark   */
{0.0, 0.0, 0.0, 1.0},/*black  */ 
};
GLubyte   fly[] = {
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x03, 0x80, 0x01, 0xC0, 0x06, 0xC0, 0x03, 0x60, 
      0x04, 0x60, 0x06, 0x20, 0x04, 0x30, 0x0C, 0x20, 
      0x04, 0x18, 0x18, 0x20, 0x04, 0x0C, 0x30, 0x20,
      0x04, 0x06, 0x60, 0x20, 0x44, 0x03, 0xC0, 0x22, 
      0x44, 0x01, 0x80, 0x22, 0x44, 0x01, 0x80, 0x22, 
      0x44, 0x01, 0x80, 0x22, 0x44, 0x01, 0x80, 0x22,
      0x44, 0x01, 0x80, 0x22, 0x44, 0x01, 0x80, 0x22, 
      0x66, 0x01, 0x80, 0x66, 0x33, 0x01, 0x80, 0xCC, 
      0x19, 0x81, 0x81, 0x98, 0x0C, 0xC1, 0x83, 0x30,
      0x07, 0xe1, 0x87, 0xe0, 0x03, 0x3f, 0xfc, 0xc0, 
      0x03, 0x31, 0x8c, 0xc0, 0x03, 0x33, 0xcc, 0xc0, 
      0x06, 0x64, 0x26, 0x60, 0x0c, 0xcc, 0x33, 0x30,
      0x18, 0xcc, 0x33, 0x18, 0x10, 0xc4, 0x23, 0x08, 
      0x10, 0x63, 0xC6, 0x08, 0x10, 0x30, 0x0c, 0x08, 
      0x10, 0x18, 0x18, 0x08, 0x10, 0x00, 0x00, 0x08};

GLubyte halftone[] = {
      0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55, 
      0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55, 
      0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
      0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55, 
      0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55, 
      0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
      0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55, 
      0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55, 
      0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
      0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55, 
      0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55, 
      0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
      0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55, 
      0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55, 
      0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
      0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55};

#ifdef  WITH_LITE
/*************************************************************************/
  void rotLitY (float *posit, float yAngle)
/*************************************************************************/
{ /*Drehe Lichtquelle um y-Achse:*/
  glMatrixMode(GL_MODELVIEW);
#ifdef  MORE_LITE
#endif//MORE_LITE
  return;
}
#endif//WITH_LITE

/*************************************************************************/
  void move(void)
/*************************************************************************/
{ /*Objekt ins Sichtvolumen verschieben:*/
  glTranslatef(0.0, 0.0, -(nah+diag/2));
  /*Objekt positionieren:*/
#ifdef  MORE_OBJ
//#else //MORE_OBJ
  glRotatef(angle[X], 1.0, 0.0, 0.0);
  glRotatef(angle[Y], 0.0, 1.0, 0.0);
  glRotatef(angle[Z], 0.0, 0.0, 1.0);
#endif//MORE_OBJ
}

/*************************************************************************/
  void drawObj (CGFobject *obj)
/*************************************************************************/
{ int jF, jP, dummyi=0;
#ifdef  WITH_LITE
  GLfloat  dumNorm3[3];
#endif//WITH_LITE

  /*Sicherstellen: Ab hier Objekt- (nicht Projektions-)Trfn.:*/
  glMatrixMode(GL_MODELVIEW);

  /*Merke Uebernahmezustand und transformiere:*/
  glPushMatrix (); // ab 10.09.10
  move();

  /*Anderer Umgang mit dem Dreieck, zu erkennen am Namen: */
  if (!strncmp (obj->Name, "Triangle", strlen("Triangle"))) {;
#ifdef  MORE_OBJ
#endif//MORE_OBJ
  }

  /*Objekt zeichnen:*/
  for (jF=0; jF < obj->nFace; jF++)
  { /*Flaechen- und Linienmuster:*/
    if (!jF) { 
#ifdef  MORE_OBJ
#endif//MORE_OBJ 
               glLineStipple(5, 0x000F); }
    else     { 
#ifdef  MORE_OBJ
#endif//MORE_OBJ 
               glLineStipple(5, 0x0FFF); }
    /*Vorlaeufige Farbgebung:*/
    if (wire == GL_FILL && shade == GL_FLAT) glColor4fv(colors[jF]);

#ifdef  WITH_LITE
    if (LitOn && wire == GL_FILL)
    { /*Material-Eigenschaften:*/
#ifdef  MORE_LITE
#endif//MORE_LITE
    }
#endif//WITH_LITE

#ifdef  WITH_TEXR
    /*Ggf. aktuelle Textur bestimmen:*/
#ifdef  MORE_TEXR
#endif//MORE_TEXR
#endif//WITH_TEXR

    glBegin(GL_POLYGON); 
    for (jP=0; jP < obj->Face[jF].nPnt; jP++)
    {
#ifdef  MORE_OBJ
#endif//MORE_OBJ 
#ifdef  WITH_LITE
#ifdef  MORE_LITE
#endif//MORE_LITE
      glNormal3fv(dumNorm3);
#endif//WITH_LITE
#ifdef  WITH_TEXR
      /*Zuordnung Eck-Texel zur naechsten Ecke:*/
      if (texOn)
      { if (jP==0) glTexCoord2f(LFT, BTM); else
        if (jP==1) glTexCoord2f(RIT, BTM); else
        if (jP==2) glTexCoord2f(RIT, TOP); else
        if (jP==3) glTexCoord2f(LFT, TOP);
      }
#endif//WITH_TEXR
      glVertex3fv(*obj->Face[jF].Pnt[jP]);
    } // Flaeche fertig
    glEnd();
  } // Objekt fertig
  glPopMatrix (); //Vergiss Trf. bis hierhin
}

#ifdef  WITH_AA
/*************************************************************************/
  void drawAAObj (CGFobject *obj)
/*************************************************************************/
{ /*Szene-AntiAliasing:*/
  int jit=0;
  float dx=0.f, dy=0.f;
#ifdef  MORE_AA
//#else   MORE_AA
  jitter_point *p2jit[7];
#endif//MORE_AA

  for (jit = 0; jit < aaJit[aaIdx]; jit++) 
  { /*Subpixel-Versatz=offset*jitter*FensterInZahlen/FensterInPixeln*/
#ifdef  MORE_AA
#endif//MORE_AA

    /*Tabula rasa im Farbpuffer: jede Einstellung nur 1x ins Bild:*/
    glClear(GL_COLOR_BUFFER_BIT);
    /*Bis anders gefordert, alles Objekt- (nicht Projektions-)Trfn.:*/
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    if (aaMod)/*Versetzte Objekte:*/
    { glPushMatrix ();
      glTranslatef (dx, dy, 0.0);
    }
#ifdef  MORE_AA
#endif//MORE_AA
    /*Einzelzeichnung:*/
    drawObj(obj);

    if (aaMod) glPopMatrix ();
    if (!jit)   glAccum(GL_LOAD,  1./aaJit[aaIdx]);
    else        glAccum(GL_ACCUM, 1./aaJit[aaIdx]);
  }
#ifdef  MORE_AA
#endif//MORE_AA
}
#endif//WITH_AA

/*************************************************************************/
  void display (void)
/*************************************************************************/
{ glClear(GL_COLOR_BUFFER_BIT);

  nah=eyez; fern=nah+2*diag;
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity(); //Tabula rasa...

  if (persp) /*Perspektive:*/
  { /*Mittige Plazierung der mit viewScale skalierten Szene:*/
    glViewport(winWidth*(1-viewScale)/2, winHeight*(1-viewScale)/2, 
               winWidth*viewScale,       winHeight*viewScale); 
    glFrustum (-diag, diag, -diag, diag, nah, fern);
  } else /*Parallel-Projektion:*/
  { glViewport(0, 0, winWidth, winHeight); 
    glOrtho (-diag, diag, -diag, diag, nah, fern);
  }
  /*Backface-Culling:*/
  if (backF) glEnable  (GL_CULL_FACE);
  else       glDisable (GL_CULL_FACE);

  /*Wireframe-AntiAliasing:*/
  if (aa && wire == GL_LINE) 
  { glEnable (GL_POINT_SMOOTH); glEnable (GL_LINE_SMOOTH);
    glEnable (GL_BLEND); glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  } else    { glDisable (GL_POINT_SMOOTH); glDisable (GL_LINE_SMOOTH);
              glDisable (GL_BLEND); }

  /*Einstellung fuer PolygonMode, Strichstaerke, Schattierung, Musterung:*/
  glPolygonMode(GL_FRONT_AND_BACK, wire);
#ifdef  MORE_OBJ
#endif//MORE_OBJ

  /*Bisherige Bewegung neu aufbauen (ohne Fehlerfortpflanzung):*/
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity(); 

  /*Objekt positionieren und zeichnen:*/
#ifdef  WITH_AA
  if (aa && wire == GL_FILL) drawAAObj(&obj); 
  else
#endif//WITH_AA
  drawObj(&obj); 

#ifdef  DBL_BUF
  glutSwapBuffers();
#else //DBL_BUF
  glFlush();
#endif//DBL_BUF
}

#ifdef  WITH_TEXR
/* *********************************************************************** */
  int checkSize (int x)
/* *********************************************************************** */
/* Make sure it's a power of 2 */
{ int j=1;

  while (j < x) j <<= 1; 
  if (j == x) return (x);
  return (0);
}

/*************************************************************************/
  void TexInit(void)
/*************************************************************************/
{ /*Textur-Einstellungen:*/
#ifdef  WITH_TEXR
  GLubyte texels[NFACE][TXWID*TXHIG][RGB];
#ifdef  MORE_TEXR
#endif//MORE_TEXR
#endif//WITH_TEXR
  int     jF=0;

  if (texOn)
  { /*Farbuebergaenge durch Texturierung, nicht durch Schattierung:*/
    glShadeModel(GL_FLAT); 
    /*Einrichtung unterscheidbarer Textur-Namen:*/
    glGenTextures(6,texName); 

    /*Individuelle Behandlung aller texturierten Flaechen:*/
    for (jF=0; jF<NFACE; jF++) 
    { /*Textur auf Normgroesse pruefen:*/
       if (!checkSize(TXWID) || !checkSize(TXHIG))
       { printf("\nwrong texture size!\n"); getchar(); }

      /*Zuerst Textur-Objekt zu jedem Namen einrichten:*/
      glBindTexture(GL_TEXTURE_2D, texName[jF]); 
#ifdef  MORE_TEXR
#endif//MORE_TEXR

      /*Fuer Texturkoordinaten >1.0 Textur dehnen:*/
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

      /*Naechstgelegene Farbe oder Farbmittelung verwenden:*/
#ifdef  MORE_TEXR
#endif//MORE_TEXR

      /*Texturfarbe deckend auftragen:*/
      glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
      //glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

      /*Daten der 2D-Textur in RGB verwenden:*/
#ifdef  MORE_TEXR
#endif//MORE_TEXR
    }
    glEnable(GL_TEXTURE_2D);
  }
  else
  { glDisable (GL_TEXTURE_2D);
  }
}
#endif//WITH_TEXR

/*************************************************************************/
  void init (void)
/*************************************************************************/
/*(Erst- & einmalige Programm-Initialisierung inkl. Laden eines Objektes:*/
{
#ifdef  MIT_LOADCGF
  if (!loadCGF (&obj, CGF_FILE11) && !loadCGF (&obj, CGF_FILE12) && 
      !loadCGF (&obj, CGF_FILE21) && !loadCGF (&obj, CGF_FILE22))
#endif//MIT_LOADCGF
  setTriangleCGF (&obj);
  printCGF (&obj);   // Werte geladener Struktur-Variablen ausgeben
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity(); //Tabula rasa...

  /*Raumdiagonale der Bounding Box:*/
  diag = BBox(&obj);

  glClearColor(0.0, 0.0, 0.0, 0.0);

#ifdef  WITH_AA
  glClearAccum(0.0, 0.0, 0.0, 0.0);
#endif//WITH_AA

  /*Tastendruck simulieren, um Menue auszugeben:*/
  key(' ', 0, 0);

  return;
}

#ifdef  WITH_LITE
/*************************************************************************/
  void LitInit(void)
/*************************************************************************/
{ /*Licht-Einstellungen:*/
  GLfloat LitAmbi[] = { 0.1, 0.1, 0.1, 1.0 };
  GLfloat LitDiff[] = { 1.0, 1.0, 1.0, 1.0 };
  GLfloat LitSpec[] = { 1.0, 1.0, 1.0, 1.0 };

  if (LitOn)
  { LitPos0[0] = -1.; LitPos0[1] = LitPos0[2] = 1.0;
    LitPos0[3] = .0;

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glShadeModel (GL_SMOOTH); //muss sein
    glEnable(GL_NORMALIZE);

    if (LitAngY != 0.) rotLitY (LitPos0,  LitAngY);
    else glLightfv(GL_LIGHT0, GL_POSITION, LitPos0);


    /* Entbehrlich (vgl. def.): */
    glLightfv(GL_LIGHT0, GL_AMBIENT, LitAmbi);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, LitDiff);
    glLightfv(GL_LIGHT0, GL_SPECULAR, LitSpec); 
  } else
  { glDisable(GL_LIGHT0);
    glDisable(GL_LIGHTING);
    glShadeModel (GL_FLAT); //Geschmacksache
  }
}
#endif//WITH_LITE

/*************************************************************************/
  void initPos(CGFobject *obj)
/*************************************************************************/
/*Erstmalige Positionierung und BBox-Berechnung eines geladenen Objekts:*/
{ printCGF (obj);  backF = 0;  eyez = 10.f;  viewScale=1.f;
  persp=1; aa=0; shade=GL_FLAT; stipple=0; 
  wire=GL_LINE;  glColor3f(1.,1.,1.); 
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity(); //Tabula rasa...
  angle[X] = angle[Y] = angle[Z] =0.; 
  diag = BBox(obj);
#ifdef  WITH_LITE
  LitOn=0; LitAngY=0.; LitInit(); 
#endif//WITH_LITE
  return;
}

/*************************************************************************/
  void key (unsigned char key, int x, int y)
/*************************************************************************/
/*Menue und Eingabe-Behandlung:*/
{ switch (key)
  { case ESC: exit(0);
#ifdef  WITH_TEXR
    case SPC: if (texOn) { texFil = GL_LINEAR+GL_NEAREST-texFil;
                           TexInit(); }                           break; 
#endif//WITH_TEXR
#ifdef MIT_LOADCGF
    case '0': if (loadCGF (&obj, CGF_FILE01) || loadCGF (&obj, CGF_FILE02))
              { initPos(&obj);
#ifdef  WITH_LITE
                LitOn=0;  LitInit();  LitAngY=0.;
#endif//WITH_LITE
#ifdef  WITH_TEXR
                texOn=0;  TexInit();
#endif//WITH_TEXR
              } else { glClear(GL_COLOR_BUFFER_BIT);
                       printf ("\n\n\r  LOAD FAILED !"); _getch(); }
              break;
    case '1': if (loadCGF (&obj, CGF_FILE11) || loadCGF (&obj, CGF_FILE12))
              { initPos(&obj);
#ifdef  WITH_LITE
                LitOn=0;  LitInit();  LitAngY=0.;
#endif//WITH_LITE
#ifdef  WITH_TEXR
                texOn=0;  TexInit();
#endif//WITH_TEXR
              } else { glClear(GL_COLOR_BUFFER_BIT);
                       printf ("\n\n\r  LOAD FAILED !"); _getch(); }
              break;
    case '2': if (loadCGF (&obj, CGF_FILE21) || loadCGF (&obj, CGF_FILE22))
              { initPos(&obj);
#ifdef  WITH_LITE
                LitOn=0;  LitInit();  LitAngY=0.;
#endif//WITH_LITE
#ifdef  WITH_TEXR
                texOn=0;  TexInit();
#endif//WITH_TEXR
              } else { glClear(GL_COLOR_BUFFER_BIT);
                       printf ("\n\n\r  LOAD FAILED !"); _getch(); }
              break;
    case '4': if (loadCGF (&obj, CGF_FILE41) || loadCGF (&obj, CGF_FILE42))
              { initPos(&obj);
#ifdef  WITH_LITE
                LitOn=0;  LitInit();  LitAngY=0.;
#endif//WITH_LITE
#ifdef  WITH_TEXR
                texOn=0;  TexInit();
#endif//WITH_TEXR
              } else { glClear(GL_COLOR_BUFFER_BIT);
                       printf ("\n\n\r  LOAD FAILED !"); _getch(); }
              break;
#endif//MIT_LOADCGF
    case '3': if (setTriangleCGF (&obj)) 
              { initPos(&obj);
#ifdef  WITH_LITE
                LitOn=0;  LitInit();  LitAngY=0.;
#endif//WITH_LITE
#ifdef  WITH_TEXR
                texOn=0;  TexInit();
#endif//WITH_TEXR
              } else { glClear(GL_COLOR_BUFFER_BIT);
                       printf ("\n\n\r  LOAD FAILED !"); _getch(); }
              break;
#ifdef  WITH_AA
    case '<': if (aa && wire == GL_FILL) aaOff*=.9f;              break; 
    case '>': if (aa && wire == GL_FILL) aaOff*=1.1f;             break; 
    case '-': if (aa && wire == GL_FILL) aaIdx = MAX(aaIdx-1,0);  break; 
    case '+': if (aa && wire == GL_FILL) aaIdx = MIN(aaIdx+1,6);  break; 

    case 'a': aa = 1 - aa; if (aa) aaOff=1.f;                     break;
    case 'A': if (aa && wire == GL_FILL) aaMod = 1 - aaMod;       break;
#else //WITH_AA
    case 'a': if (wire == GL_LINE) aa = 1 - aa;  else aa = 0;     break;
#endif//WITH_AA
    case 'b': backF = 1-backF;                                    break;
    case 'e': eyez *=.9f;  viewScale /= .9f;                      break;
    case 'E': eyez *=1.1f; viewScale /= 1.1f;                     break;
#ifdef  WITH_LITE
    case 'l': if (wire == GL_FILL)
              { LitOn=1-LitOn;  LitAngY=0.; 
                if (LitOn){ shade=GL_SMOOTH;}
                else      { shade=GL_FLAT; }  LitInit(); }        break;
    case 'L': if (LitOn) { LitAngY += 30.;
                 if (LitAngY >= 360) LitAngY -= 360; LitInit(); } break;
#endif//WITH_LITE
    case 'p': persp = 1-persp;                                    break;
    case 's': if (wire == GL_FILL) 
              shade = GL_SMOOTH+GL_FLAT-shade;                    break;
    case 't': stipple = 1-stipple;
#ifdef  WITH_TEXR
                texOn=0;  TexInit();
#endif//WITH_TEXR
                                                                  break;
#ifdef  WITH_TEXR
    case 'T': if (wire == GL_LINE)                                break;
              texOn = 1-texOn; 
#ifdef  WITH_LITE
              LitOn=0;  LitAngY=0.; 
#endif//WITH_LITE
              if (texOn) { stipple = 0; shade = GL_FLAT; }
              TexInit();                                          break;
#endif//WITH_TEXR
    case 'w': wire  = GL_LINE+GL_FILL - wire; 
              if (wire == GL_FILL) 
              { backF = 1; 
#ifndef WITH_AA
                aa = 0; 
#endif//WITH_AA
              } else 
              { glColor3f(1.,1.,1.);  shade = GL_FLAT; 
#ifdef  WITH_LITE
                LitOn=0;  LitInit();  LitAngY=0.;
#endif//WITH_LITE
#ifdef  WITH_TEXR
                texOn=0;  TexInit();
#endif//WITH_TEXR
              }                                                   break;
    case 'x': angle[X] -= 5.; if (angle[X] <=-360) angle[X]+=360; break;
    case 'X': angle[X] += 5.; if (angle[X] >= 360) angle[X]-=360; break;
    case 'y': angle[Y] -= 5.; if (angle[Y] <=-360) angle[Y]+=360; break;
    case 'Y': angle[Y] += 5.; if (angle[Y] >= 360) angle[Y]-=360; break;
    case 'z': angle[Z] -= 5.; if (angle[Z] <=-360) angle[Z]+=360; break;
    case 'Z': angle[Z] += 5.; if (angle[Z] >= 360) angle[Z]-=360; break;
  }
  system (CON_CLS);
  printf ("\r Press (keeping the GLUT window activated):");
  printf ("\n\n\r <ESC> to quit");
#ifdef   MIT_LOADCGF
    printf ("\n\r 0...4    load Xface/cube/house/triang./cylinder");
#endif //MIT_LOADCGF
#ifdef  WITH_AA
  printf ("\n\r a        toggle AntiAliasing");
#else //WITH_AA
  printf ("\n\r a        toggle wire frame AntiAliasing ");
#endif//WITH_AA
  if (aa) printf ("(ON)"); else printf ("(OFF)"); 
#ifdef  WITH_AA
  if (aa && wire == GL_FILL) {
  printf ("\n\r          </> decr./incr.pixelOffset(curr.:%.2f)", aaOff);
  printf ("\n\r          -/+ less/more samples (curr.:%2d)", aaJit[aaIdx]);
  printf ("\n\r          A   toggle Modelview");  
  if (aaMod) printf ("(ON)"); printf ("/ProjectionMode"); 
  if (!aaMod) printf ("(ON)"); }
#endif//WITH_AA
  printf ("\n\r b        toggle Backface culling");
  if (backF) printf ("(ON)"); else printf ("(OFF)"); 
  printf ("\n\r e / E    decrease/increase Eye point z-coord.");
#ifdef  WITH_LITE
  printf ("\n\r l / L    toggle light");
  if (LitOn) printf ("(ON)"); else printf ("(OFF)"); 
  printf ("/rot.Light rd.Y(%3.0f)", LitAngY);
#endif//WITH_LITE
  printf ("\n\r p        toggle projection: Persp."); 
  if (persp) printf ("(ON)");
  printf ("/orth."); if (!persp) printf ("(ON)");
  printf ("\n\r s        toggle Shading: smooth"); 
  if (shade == GL_SMOOTH) printf ("(ON)");
  printf ("/flat"); if (shade == GL_FLAT) printf ("(ON)");
  printf ("\n\r t        toggle sTippling");
  if (stipple) printf ("(ON)"); else printf ("(OFF)"); 
#ifdef  WITH_TEXR
  printf ("\n\r T        toggle Texturing");
  if (texOn) printf ("(ON)"); else printf ("(OFF)"); 
  if (texOn) { 
  printf ("\n\r          <SPC> toggle filter: lin.");
  if (texFil == GL_LINEAR) printf ("(ON)"); 
  printf ("/nearest"); if (texFil == GL_NEAREST) printf ("(ON)");
 }
#endif//WITH_TEXR
  printf ("\n\r w        toggle PolygonMode: Wire"); 
  if (wire == GL_LINE) printf ("(ON)");
  printf ("/fill"); if (wire == GL_FILL) printf ("(ON)");
  printf ("\n\r x / X    decrease/increase X-rotation angle");
  printf ("\n\r y / Y    decrease/increase Y-rotation angle");
  printf ("\n\r z / Z    decrease/increase Z-rotation angle");
  printf ("\n\n\r Current values: eyez    =%7.2f", eyez);
  printf ("\n\r                 angle[X]=%7.2f", angle[X]);
  printf ("\n\r                 angle[Y]=%7.2f", angle[Y]);
  printf ("\n\r                 angle[Z]=%7.2f", angle[Z]);
  printf ("\n\r");
  display();
  return;
}

/*************************************************************************/
  int main (int argc, char **argv)
/*************************************************************************/
{ glutInit(&argc, argv);
/*Reihenfolge GLUT_ACCUM, GLUT_DOUBLE einhalten:*/
#ifdef  WITH_AA
  glutInitDisplayMode (GLUT_ACCUM);
#endif//WITH_AA
#ifdef  DBL_BUF
  glutInitDisplayMode(GLUT_DOUBLE);
#endif//DBL_BUF
  glutInitWindowSize(winWidth, winHeight);
  glutCreateWindow("CG goes OpenGL");
  glutDisplayFunc(display);
  glutKeyboardFunc(key);
  init(); 
  glutMainLoop();
  return 0;
}
#ifdef  EVEN_MORE
/* Zur Berechnung von viewScale:
Verkuerzung von eyez bewirkt (bei konstanten glFrustum-Parametern)
eine Oeffnung des Sichtwinkels, so dass:
winWidth(neu) / winWidth(alt) = viewScale
Da die Fensterbreiten mittig "uebereinander gelegt" werden, 
gilt fuer die Startkoordinate x:
x(neu) = (winWidth(alt)-winWidth(neu)) / 2
(winHeight und y entsprechend)
max.Werte (danach Plazierung nicht mittig!?):
glViewport(-1900,-1900,4100,4100); */
#endif//EVEN_MORE
