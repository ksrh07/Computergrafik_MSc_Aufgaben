/* conDraw.h */
#ifndef CONDRAW_H
  #define CONDRAW_H

  #include <stdio.h>
  #include <stdlib.h>

  /*Konsole-Ma�e:*/
//  #define GROSSBILD
  #ifdef  GROSSBILD
    #define CON_YMAX   68 //Index letzte beschreibbare Zeile (0-)
    #define CON_SIZE  "mode 139, 70" //Spalten+Platz fuer Cursor, Zeilen+Leerzeile
  #else //GROSSBILD
    #define CON_YMAX   23 //Index letzte beschreibbare Zeile (0-23)
    #define CON_SIZE  "mode 49, 25" //Spalten+Platz fuer Cursor, Zeilen+Leerzeile
  #endif//GROSSBILD

  #define CON_YMIN    0
  #define CON_XMIN    0
  #define CON_XMAX    (2*(CON_YMAX+1)-1) //Index letzte beschr. Spalte (0-)

# ifdef unix
  #define CON_CLS "clear"
# else
  #define CON_CLS   "cls"
# endif

  /*Prototypen:*/
  void conClrBuff   (void);
  void conClrScr    (void);
  void conSetPixel  (int x, int y, char val);
  void conSwapBuff  (void);
#endif  //CONDRAW_H
