/* fillOps.c */
/* Operationen zum Fuellen von Flaechen. A. Christidis */
/* (seit Nov. 09 mit einheitlichem Fuellalgorithmus mit/ohne Spiegelung 
   des Bresenham-Linienalgorithmus) */

#include "fillOps.h" 
#include "MatrOps.h"

#ifdef  WITH_FILL
/*************************************************************************/
  void initF4F (int *endL, int *endR)
/*************************************************************************/
/* Initialize face arrays for fill */
{ int y=0;
  for (y=0; y<FACE_Y; y++)
  { endL[y] = endR[y] = -1;
  }
  return;
}

/*************************************************************************/
  void markF4F (int x, int y, int *endL, int *endR)
/*************************************************************************/
/* Mark face for fill */
{ /*Absturzgefahr bei nahem Augenpunkt, wenn Figur ueber Fenstergrenzen tritt:*/
  if (y<CON_YMIN || y>CON_YMAX) return;
  if (x < CON_XMIN) x = CON_XMIN;
  if (x > CON_XMAX) x = CON_XMAX;

#ifdef  MORE_FILL
#endif//MORE_FILL

  return;
}

/*************************************************************************/
  void fillF (int *endL, int *endR, char symbol)
/*************************************************************************/
/* Fill face (bottom up) with symbol(s) */
{ int x=0, y=0;

  /* Sprung bis zum ersten Flaechenpunkt: */
  for (y=0; y<FACE_Y; y++) if (endL[y] != -1) break; 

  /* Fuellen von unten nach oben, bis zur zweithoechsten Bildzeile: */
#ifdef  MORE_FILL
#endif//MORE_FILL
  return;
}
#endif//WITH_FILL


/*======================================================================**/
#ifdef  ZUMLOESCHEN
// Zum Loeschen
  conSwapBuff();             //Hintergrund-Puffer sichtbar machen
// Zum Loeschen
#endif//ZUMLOESCHEN
