/* MatrOps.c */
/*Operationen der Matrizenrechnung*/

#include "MatrOps.h"

#ifdef   WITH_MATR1
/* *********************************************************************** */
   int Identity (float *matrix, int dim)
/* *********************************************************************** */
/* Die quadratische <dim>x<dim>-Matrix <matrix> erhaelt die Werte
   einer Einheitsmatrix.

   return =  1:  Ausfuehrung erfolgreich

   ======= Variablen der Parameterrampe: =======
 (I/O)  matrix    Eingabe- und Ergebnismatrix (<dim> x <dim> Elemente)
 (I)    dim       Anzahl Matrix-Zeilen /-Spalten
*/
{ int jz=0, js=0;

  /* Zeilenweise Zuweisung der Elemente: */
  for (jz=0; jz<dim; jz++)
  { for (js=0; js<dim; js++)
    { if (jz == js) matrix[jz*dim+js] = 1.f;
      else          matrix[jz*dim+js] = 0.f;
    }
  }

  return (1);
}

/* *********************************************************************** */
   int matMul (float f1, float *mi1, int z1, int s1z2,
			   float f2, float *mi2, int s2, float *mout)
/* *********************************************************************** */
/* Es wird das gewichtete Produkt zweier Matrizen berechnet.
   <mout> wird eine <z1>x<s2>-Matrix

   return =  1:  Ausfuehrung erfolgreich

   ======= Variablen der Parameterrampe: =======
 (I)    f1, f2    skalare Faktoren
 (I)    mi1, mi2  Eingangsmatrizen (z1 x s1z2), (s1z2 x s2)
 (I)    z1        Anzahl Zeilen erste Matrix
 (I)    s1z2      Anzahl Spalten erste Matrix = Anzahl Zeilen zweite Matrix
 (I)    s2        Anzahl Spalten zweite Matrix
 (O)    mout      Ergebnismatrix (z1 x s2)
*/
{ 
#ifdef   MORE_MATR1
#endif //MORE_MATR1
  return (1);
}
#endif //WITH_MATR1

#ifdef   WITH_MATR2
/* *********************************************************************** */
   int matAdd (float f1, float *mi1, int z12, int s12, 
               float f2, float *mi2, float *mout)
/* *********************************************************************** */
/* Es wird die gewichtete Summe zweier <z12>x<s12>-Matrizen berechnet:
   mout = f1 * mi1 + f2 * mi2
   mout kann identisch mit einer der beiden Eingangsmatrizen (mi1, mi2) sein.

   return =  1:  Ausfuehrung erfolgreich

   ======= Variablen der Parameterrampe: =======
 (I)    f1, f2    skalare Faktoren
 (I)    mi1, mi2  Eingangsmatrizen 
 (I)    z12       Anzahl Zeilen (bei allen Matrizen)
 (I)    s12       Anzahl Spalten (bei allen Matrizen)
 (O)    mout      Ergebnismatrix
*/
{   long j1=0;

    for (j1=0; j1<z12*s12; j1++) mout[j1] = f1 * mi1[j1] + f2 * mi2[j1];

    return (1);
}

/* *********************************************************************** */
   int spV (float f1, float *vi1, float f2, float *vi2, float *out)
/* *********************************************************************** */
/* Es wird das gewichtete Skalarprodukt zweier 3D-Vektoren berechnet:
   out = <f1 * vi1 , f2 * vi2>

   return =  1:  Ausfuehrung erfolgreich

   ======= Variablen der Parameterrampe: =======
 (I)    f1, f2    skalare Faktoren
 (I)    vi1, vi2  3D-Eingangsvektoren
 (O)    out       Ergebnis (Skalar)
*/
{
    long j1=0;

    *out = 0.f;
#ifdef   MORE_MATR2
#endif //MORE_MATR2

    return (1);
}

/* *********************************************************************** */
   int vpV (float f1, float *vi1, float f2, float *vi2, float *vout)
/* *********************************************************************** */
/* Es wird das gewichtete Vektorprodukt zweier Vektoren berechnet:
   vout(x,y,z) = f1 * vi1(x,y,z) x f2 * vi2(x,y,z)
   vout kann identisch mit einem der beiden Eingangsvektoren (vi1, vi2) sein.

   return =  1:  Ausfuehrung erfolgreich

   ======= Variablen der Parameterrampe: =======
 (I)    f1, f2    skalare Faktoren
 (I)    vi1, vi2  3D-Eingangsvektoren
 (O)    vout      3D-Ergebnisvektor
*/
{   int j1;
    float dummyf=0, dummyf3[3];

    dummyf = f1 * f2;
    dummyf3[0] = (vi1[1] * vi2[2] - vi1[2] * vi2[1]) * dummyf;
#ifdef   MORE_MATR2
#endif //MORE_MATR2

    return (1);
}
#endif //WITH_MATR2

#ifdef   WITH_MATR3
/* *********************************************************************** */
   int saveMatAs (float *origMat, float *destMat, int dim)
/* *********************************************************************** */
/* (Die ersten) <dim> Elemente der Matrix <origMat> werden in Matrix <destMat>
   kopiert. Fuer beide Matrizen muss jeweils Speicherplatz existieren.

   return =  1:  Ausfuehrung erfolgreich

   ======= Variablen der Parameterrampe: =======
 (I)    origMat   Eingangsmatrix
 (I)    dim       (Mindest-)Anzahl zu uebertragender Elemente 
 (O)    destMat   Zielmatrix
*/
{ int j;
  for (j=0; j<dim; j++) destMat[j] = origMat[j];
  return (1);
}
#endif //WITH_MATR3
