/* conDraw.c */
/* Zeichnen im Konsole-Fenster. A. Christidis */

#include "conDraw.h"
# ifdef unix
  # include <ncurses.h>
  # define printf printw
# endif

/*Globale Variablen: */
char  conBuffer[CON_YMAX+1][CON_XMAX+2]; /* [CON_XMAX+1 + '\0'] */

/*************************************************************************/
  void conClrBuff (void)
/*************************************************************************/
/*Den gesamten verwendeten Puffer mit Leerzeichen fuellen:*/
{ int  j1=0,j2=0;

  for (j1=CON_YMIN; j1<=CON_YMAX; j1++)
  { for (j2=CON_XMIN; j2<=CON_XMAX; j2++) 
    { conBuffer[j1][j2] = '\040';  //Wirkungsgleich: ' ' und '\040'!
    }
  }
  refresh();
  return;
}

/*************************************************************************/
  void conClrScr (void)
/*************************************************************************/
/*Konsole-Fenster loeschen:*/
{
# ifdef unix
  clear();
# else
  system (CON_CLS);
# endif
  return;
}

/*************************************************************************/
  void conSwapBuff (void)
/*************************************************************************/
/*Puffer-Inhalt auf Konsole-Fenster ausgeben:*/
{ int  j1=0;
  /*Erste + letzte beschreibbare Stelle (zur Kontrolle):*/
  /*conBuffer[CON_YMAX][CON_XMIN] = '>';
    conBuffer[CON_YMIN][CON_XMAX] = '<'; */

  conClrScr();
  for (j1=CON_YMAX; j1>=CON_YMIN; j1--)
  { printf (conBuffer[j1]);  printf ("\n\r"); 
  }
  return;
}

/*************************************************************************/
  void conSetPixel (int x, int y, char val)
/*************************************************************************/
/*ASCII-Zeichen als einzelnes Pixel setzen:*/
{ if (y<CON_YMIN || y>CON_YMAX || x<CON_XMIN || x>CON_XMAX) return;
  conBuffer[y][x] = val;
  return;
}
