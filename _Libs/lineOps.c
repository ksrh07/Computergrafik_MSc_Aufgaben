/* lineOps.c */
/* Operationen zum Zeichnen gerader Linien. A. Christidis */

#include "lineOps.h" 
#include "fillOps.h"

/*************************************************************************/
  void MidpointLine (int x0, int y0, int xn, int yn, char symbol)
/*************************************************************************/
{ int dx, dy, d;
  int incrE, incrNE; /*Increments for move to E and NE*/
  int  x=x0, y=y0; /*Start & current pixel*/

  dx=xn-x0; dy=yn-y0; d=2*dy-dx;
  incrE=2*dy; incrNE=2*(dy-dx); /*Increments for move to E and NE*/

  for (x=x0; x < xn; x++)
  {
    markF4F(x, y, endL, endR);
    conSetPixel (x, y, symbol);
    if (d < 0) { d += incrE; }       /*weiter gen  O*/
    else       { d += incrNE; y++; } /*weiter gen NO*/
  }
  return;
}


/*************************************************************************/
//Brute-force algorithm:
  void BruteForceLine (int x0, int y0, int xn, int yn, char symbol)
/*************************************************************************/
{ int x=x0;
  float y=(float)y0, m=(float)(yn-y0)/(xn-x0);
  
  for (x=x0; x <= xn; x++, y+=m)
      conSetPixel (x, (int)y, symbol);

  return;
}
