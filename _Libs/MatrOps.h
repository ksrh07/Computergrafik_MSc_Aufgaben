/*                                MatrOps.h                                 */
#ifndef MATROPS_H
  #define MATROPS_H

  #define WITH_MATR1
  #define MORE_MATR1

  #define WITH_MATR2
  #define MORE_MATR2

  #define WITH_MATR3
  #define MORE_MATR3

  #include  <stdio.h>
  #include  <math.h>
  #include  <stdlib.h>

  /*Prototypen:*/
#ifdef   WITH_MATR1
   int   Identity  (float *matrix, int dim);
   int   matMul    (float f1, float *mi1, int z1, int s1z2,
			        float f2, float *mi2, int s2, float *mout);
#endif //WITH_MATR1

#ifdef   WITH_MATR2
   int   matAdd    (float f1, float *mi1, int z12, int s12, 
                    float f2, float *mi2, float *mout);
   int   spV       (float f1, float *vi1, float f2, float *vi2, float *out);
   int   vpV       (float f1, float *vi1, float f2, float *vi2, float *vout);
#endif //WITH_MATR2

#ifdef   WITH_MATR3
   int   saveMatAs (float *origMat, float *destMat, int dim);
#endif //WITH_MATR3

#endif //MATROPS_H

