/* shadOps.h */
/* Operationen zum Schattieren von Flaechen. A. Christidis */
#ifndef SHADOPS_H
  #define SHADOPS_H

/*-----------------------------------------------------------------------*/
  #define WITH_SHAD
  #define MORE_SHAD

  #ifdef  MORE_SHAD
    #define WITH_SHAD
  #endif//MORE_SHAD

  #ifdef  WITH_SHAD
     #define  WITH_FILL 
     #define  WITH_GLEVEL
  #endif//WITH_SHAD
/*-----------------------------------------------------------------------*/

  #include "conDraw.h"
  #include "fillOps.h" 
  #include "GrafOps.h" 
  #include "interOps.h"
  #include "LoadCGF.h" //enthaelt Grauton-Definition
  #include "MatrOps.h"

  /*Globale Variablen:*/
  int   shade, colIdL[FACE_Y], colIdR[FACE_Y];

  /*Prototypen:*/
  float reflex    (float *light, float *normal, float ambient);
  int   turnRndXY (float *point, float *xyAngle);
#endif//SHADOPS_H
