/* GrafOps.c */
/*Operationen der Computer-Grafik*/

#define MORE_GRAPH1
#define MORE_GRAPH2
#define MORE_GRAPH3

#include "GrafOps.h" 

#ifdef   WITH_GRAPH1
/* *********************************************************************** */
   int rotate (float theta, int axis, float *posMat)
/* *********************************************************************** */
/* Rotation eines Objektes durch Multiplikation seiner 4x4-
   Positionierungsmatrix von links mit einer Rotationsmatrix

   return =  1:  Ausfuehrung erfolgreich

   ======= Variablen der Parameterrampe: =======
 (I)    axis       Kennzahl fuer Bezugsachse
		axis = 0:  Rotation um x-Achse
		axis = 1:  Rotation um y-Achse
		axis = 2:  Rotation um z-Achse
 (I)    theta      Winkel der Rotation in Grad
 (I/O)  posMat     Positionierungsmatrix 
*/
{ const  double  GRAD  = atan(1.) / 45.;
  int j1=0;
  double sin_theta=0, cos_theta=1;
  float  rotMat[4*4], dummy[4*4];
  /* Plausibilitaetskontrollen: */
  if (axis < 0 || axis >2) return (-1);

  sin_theta = sin (theta*GRAD);
  cos_theta = cos (theta*GRAD);

  /* Aufbau der Rotationsmatrix:*/
  Identity (rotMat, 4);
#ifdef   MORE_GRAPH1
#endif //MORE_GRAPH1

  /* Bisherige Positionierung von links multiplizieren: */
  matMul (1., rotMat, 4, 4, 1., posMat, 4, dummy);
  for (j1=0; j1<4*4; j1++) posMat[j1] = dummy[j1];

  return (1);
}

/* *********************************************************************** */
   int scale (float sx, float sy, float sz, float *posMat)
/* *********************************************************************** */
/* Skalierung eines Objektes durch Multiplikation seiner 4x4-
   Positionierungsmatrix von links mit einer Skalierungsmatrix

   return =  1:  Ausfuehrung erfolgreich

   ======= Variablen der Parameterrampe: =======
 (I)    sx,sy,sz   Skalierung in x-, y-, z-Richtung
 (I/O)  posMat     Positionierungsmatrix 
*/
{ int j1=0;
  float  scalMat[4*4], dummy[4*4];

  /* Aufbau der Skalierungsmatrix:*/
  Identity (scalMat, 4);
#ifdef   MORE_GRAPH1
#endif //MORE_GRAPH1

  /* Bisherige Positionierung von links multiplizieren: */
  matMul (1., scalMat, 4, 4, 1., posMat, 4, dummy);
  for (j1=0; j1<4*4; j1++) posMat[j1] = dummy[j1];

  return (1);
}

/* *********************************************************************** */
   int translate (float xt, float yt, float zt, float *posMat)
/* *********************************************************************** */
/* Translation eines Objektes durch Multiplikation seiner 4x4-
   Positionierungsmatrix von links mit einer Translationsmatrix

   return =  1:  Ausfuehrung erfolgreich

   ======= Variablen der Parameterrampe: =======
 (I)    xt,yt,zt   Verschiebungen in x-, y-, z-Richtung
 (I/O)  posMat     Positionierungsmatrix 
*/
{ int j1=0;
  float  transMat[4*4], dummy[4*4];

  /* Aufbau der Translationsmatrix:*/
  Identity (transMat, 4);
#ifdef   MORE_GRAPH1
#endif //MORE_GRAPH1

  /* Bisherige Positionierung von links multiplizieren: */
  matMul (1., transMat, 4, 4, 1., posMat, 4, dummy);
  for (j1=0; j1<4*4; j1++) posMat[j1] = dummy[j1];

  return (1);
}
#endif //WITH_GRAPH1

#ifdef   WITH_GRAPH2
/* *********************************************************************** */
   int backFcull (float *vrtx1, float *vrtx2, float *vrtx3, float *eye)
/* *********************************************************************** */
/* Ermittlung der vom Augenpunkt abgewandten Objektflaechen; Eckpunkte 
   vrtx1, vrtx2 u. vrtx3 muessen gegen den Uhrzeigersinn angeordnet sein.

   return =  BACK:  Flaeche abgewandt (verdeckt)
   return =  SHOW:  Flaeche sichtbar

   ======= Variablen der Parameterrampe: =======
 (I)    vrtx1, vrtx2, vrtx3  Flaechen-Eckpkte (3- o. mehrdimensional)
 (I)    eye                  Augenpunkt (3- o. mehrdimensional)
*/
{ float cull=0., edge1[3], edge2[3], normal[3];
#ifdef  ENTWEDER
   normalF (vrtx1, vrtx2, vrtx3, normal);
#else//ODER
  matAdd (1., vrtx3, 3, 1, -1., vrtx2, edge1);
  matAdd (1., vrtx1, 3, 1, -1., vrtx2, edge2);
  vpV (1., edge1, 1., edge2, normal);
#endif//ENTWEDER ODER
#ifdef   MORE_GRAPH2
#endif //MORE_GRAPH2

  if (cull < 0.) return BACK;
  else           return SHOW;
}

/* *********************************************************************** */
   int perspTrf_z0 (float eyez, float *posMat)
/* *********************************************************************** */
/* Perspektivische Transformation eines Objektes auf die Ebene z=0 durch 
   Multiplikation seiner 4x4-Positionierungsmatrix von links mit einer 
   Projektionsmatrix

   return =  1:  Ausfuehrung erfolgreich

   ======= Variablen der Parameterrampe: =======
 (I)    eyez       z-Koordinate des Projektionszentrums 
 (I/O)  posMat     Positionierungsmatrix 
*/
{ int j1=0;
  float  perspMat[4*4], dummy[4*4];

  /* Aufbau der Transformationsmatrix:*/
  Identity (perspMat, 4);
#ifdef   MORE_GRAPH2
#endif //MORE_GRAPH2

  /* Bisherige Positionierung von links multiplizieren: */
  matMul (1., perspMat, 4, 4, 1., posMat, 4, dummy);
  for (j1=0; j1<4*4; j1++) posMat[j1] = dummy[j1];

  return (1);
}
#endif //WITH_GRAPH2

#ifdef   WITH_GRAPH3
/* *********************************************************************** */
   int normalF (float *vrtx1, float *vrtx2, float *vrtx3, float *normal)
/* *********************************************************************** */
/* Ermittlung der Normalen einer Flaeche, die durch 3 Eckpunkte definiert ist.
   vrtx1, vrtx2 u. vrtx3 muessen gegen den Uhrzeigersinn angeordnet sein.
   Der Ergebnisvektor ist nicht normiert!

   return =  1:  Ausfuehrung erfolgreich

   ======= Variablen der Parameterrampe: =======
 (I)    vrtx1, vrtx2, vrtx3  Flaechen-Eckpkte (3- o. mehrdimensional)
 (O)    normal               Normalen-Vektor (3- o. mehrdimensional, nicht normiert)
*/
{ float edge1[3], edge2[3];

  matAdd (1., vrtx3, 3, 1, -1., vrtx2, edge1);
  matAdd (1., vrtx1, 3, 1, -1., vrtx2, edge2);
#ifdef   MORE_GRAPH3
#endif //MORE_GRAPH3

  return (1);
}

/* *********************************************************************** */
   float normV (float *vec)
/* *********************************************************************** */
/* Normierung eines Vektors auf Laenge 1. Auswertung von genau 3 Dimensionen.

   return     :  Betrag des Vektors

   ======= Variablen der Parameterrampe: =======
 (I/O)  vec        zu normierender Vektor (3- o. mehrdimensional)
*/
{ float val=0.f;
#ifdef   MORE_GRAPH3
#endif //MORE_GRAPH3
  return (val);
}
#endif //WITH_GRAPH3
