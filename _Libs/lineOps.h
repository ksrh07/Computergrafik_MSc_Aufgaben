/* lineOps.h */
/* Operationen zum Zeichnen gerader Linien. A. Christidis */
#ifndef LINEOPS_H
  #define LINEOPS_H

  #include "conDraw.h"

  void MidpointLine (int x0, int y0, int x1, int y1, char symbol);

#endif  //LINEOPS_H
