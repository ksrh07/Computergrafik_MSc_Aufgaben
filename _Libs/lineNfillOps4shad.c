/* lineNfillOps4shad.c */
/* Operationen zum Zeichnen gerader Linien und Fuellen von Polygonen. 
   A. Christidis */


  #include "fillOps.h"
  #include "interOps.h"
  #include "shadOps.h" 

  #include "lineOps.c" 
  #include "fillOps.c"
  #include "interOps.c"
