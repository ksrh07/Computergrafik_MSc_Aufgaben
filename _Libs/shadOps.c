/* shadOps.c */
/* Operationen zum Schattieren von Flaechen. A. Christidis */

#include "shadOps.h" 
#ifdef  WITH_SHAD
/*************************************************************************/
  float reflex (float *light, float *normal, float ambient)
/*************************************************************************/
/* Calculate and return the (scalar) intensity of reflected light at face 
   with normal vector <normal>, lit by light given by vector <light> 
   (pointing in the opposite direction), and ambient component <ambient>. 
   The coefficients for diffuse and ambient reflection are taken to unity 
   (rho=1.0). The normal vector is assumed to be of unity length 
   (normalized). The resulting reflected light may be stronger than 
   the incident. 
*/
{ float _reflex=0.f;

#ifdef  MORE_SHAD
#endif//MORE_SHAD

  _reflex += ambient;
  if (_reflex < ambient)
  { printf ("\rERROR IN reflex!  (CR to continue) "); getchar(); }
  return (_reflex);
}

/*************************************************************************/
  int turnRndXY (float *point, float *xyAngle)
/*************************************************************************/
/*Turn the passed <NDIM> dimensional point vector <point> around the x and 
  y axes by the angles indicated by <xyAngle[X]> and <xyAngle[Y]>*/
{
#ifdef  MORE_SHAD
#endif//MORE_SHAD

  return OK;
}
#endif//WITH_SHAD
