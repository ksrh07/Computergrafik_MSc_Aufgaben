/* fillOps.h */
/* Operationen zum Fuellen von Flaechen. A. Christidis */
#ifndef FILLOPS_H
  #define FILLOPS_H

  #include "conDraw.h"
  #include "lineOps.h" 

  #define MORE_FILL
  #define WITH_FILL

  /*max. Anzahl Pixel einer Kante (Schattierung, Texturierung):*/
  #define LINLEN MAX(CON_YMAX-CON_YMIN,CON_XMAX-CON_XMIN)+1 
  /*max. Anzahl angezeigter Zeilen einer Objektflaeche:*/
  #define FACE_Y  (CON_YMAX - CON_YMIN+1)

  #define KO       0
  #define OK       1
  #define G_LEVELS 4

  /*Globale Variablen:*/
  int  endL[FACE_Y], endR[FACE_Y];
  char grey[G_LEVELS];// = {DGREY, MGREY, LGREY, WHITE}; // s. conInit()

  /*Prototypen:*/
  void  fillF   (int *endL, int *endR, char symbol);
  void  initF4F (int *endL, int *endR);
  void  markF4F (int x, int y, int *endL, int *endR);

#endif//FILLOPS_H
