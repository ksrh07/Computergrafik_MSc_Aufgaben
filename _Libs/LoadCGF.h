/* LoadCGF.h */
#ifndef CGFOBJ_H
  #define CGFOBJ_H

  #include <stdio.h>
  #include <stdlib.h>
  #include <string.h>

  #pragma warning(disable : 4996) // ("veraltet")

  #define NDIM        4
  #define LENGTH     80
  #define BLACK    '\040'      // schwarzes Zeichen
  #define DGREY    '\260'      // dunkelgraues Zeichen
  #define MGREY    '\261'      // mittelgraues Zeichen
  #define LGREY    '\262'      // leichtgraues Zeichen
  #define WHITE    '\333'      // weisses Zeichen

  enum {X=0, Y=1, Z=2, W=3};

  typedef char  String[LENGTH];

  typedef struct
  { int     nPnt;              // Anzahl Eckpunkte
    float ***Pnt;              // Punkt-Indizes
    char    symbol;            // Zeichen f. Konsole
  }
  Polygon;

  typedef struct
  { String  Name;              // Objekt-Name

    int     nVrtx;             // Anzahl Objektpunkte
    int     nFace;             // Anzahl Flaechen
    float  **Vrtx;             // Objektpunktkoord.
    Polygon *Face;             // Flaechenliste

    float   posMat[NDIM*NDIM]; // Positionsmatrix 
  }
  CGFobject;

  /*Prototypen:*/
  int   loadCGF        (CGFobject *obj, char *filnam);
  int   printCGF       (CGFobject *obj);
  int   setTriangleCGF (CGFobject *obj);

#endif //CGFOBJ_H

