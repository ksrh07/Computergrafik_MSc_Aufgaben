/* auxOps.h */
#ifndef AUXOPS_H
  #define AUXOPS_H

  #define MORE_AUX

  #include <math.h>   //wg. sqrt()
  #include "LoadCGF.h"

  /*Prototypen:*/
  float BBox (CGFobject *obj);

#endif //AUXOPS_H
