/*                                GrafOps.h                                 */
#ifndef GRAFOPS_H
  #define GRAFOPS_H

  #define WITH_GRAPH1
  #define WITH_GRAPH2
  #define WITH_GRAPH3

  #include <math.h>
  #include "MatrOps.h" 

  #define BACK  1
  #define SHOW  0

  /*Prototypen:*/
#ifdef   WITH_GRAPH1
  int rotate      (float theta, int axis, float *posMat);
  int scale       (float sx, float sy, float sz, float *posMat);
  int translate   (float xt, float yt, float zt, float *posMat);
#endif //WITH_GRAPH1

#ifdef   WITH_GRAPH2
  int backFcull   (float *vrtx1, float *vrtx2, float *vrtx3, float *eye);
  int perspTrf_z0 (float eyez, float *posMat);
#endif //WITH_GRAPH2

#ifdef   WITH_GRAPH3
  int normalF     (float *vrtx1, float *vrtx2, float *vrtx3, float *normal);
  float normV     (float *vec);
#endif //WITH_GRAPH3

#endif  //GRAFOPS_H
