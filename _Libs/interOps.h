/* interOps.h */
/* Operationen zur linearen Interpolation. A. Christidis */
#ifndef INTEROPS_H
  #define INTEROPS_H
  #include <math.h>  //wg. abs()
  #include <stdio.h> //wg. printf()

  #ifndef MIN
    #define MIN(x,y) (( (x) < (y) ) ? (x) : (y))
  #endif  MIN
  #ifndef MAX  
    #define MAX(x,y) (( (x) > (y) ) ? (x) : (y))
  #endif  MAX

  #define DEBUG_LERP
  #define MORE_LERP

  #define KO       0
  #define OK       1

  /*Prototypen:*/
  float lerp      (float a, float b, float t);
  int   set_iLerp (int Dx, int y0, int yn, int *yStore);

#ifdef  WITH_GLEVEL
//Versionen mit anders interpretierter Parameterliste:
//int   set_iLerp (int edgeLen, int colIdx0, int colIdxN, int *linColId);
  void  chklerp   (float a, float b, float dt);
  int   gLevel    (float intens, int nLevels);
  int   ilerp     (int first, int last, float t);
#endif//WITH_GLEVEL

#endif//INTEROPS_H
