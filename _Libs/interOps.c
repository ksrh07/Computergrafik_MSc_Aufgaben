/* interOps.c */
/* Operationen zur linearen Interpolation. A. Christidis */

#include "interOps.h"
/*************************************************************************/
  int set_iLerp (int Dx, int y0, int yn, int *yStore)
/*************************************************************************/
/*Linear interger interpolation over the number of <Dx> samples between 
  the values <y0> and <yn> after the Bresenham line algorithm. 
  The resulting <Dx> values are stored in the array <yStore>.*/
{ int dx=0, dy=0, Dy=0, dv=0, incrX=0, incrY=0;
  int x=0, y=y0, incr=1;

  if (Dx < 2)  return KO; 

  dy = abs(yn-y0); dx = Dx-1; /*Distances betw. first & last sample*/
  Dy = dy+1;                  /*Number of different values to store*/
  yStore[dx] = yn;

  if (dx > dy)
  { dv = dy-dx; /*Decision variable for y changes*/
    incrX=Dy; incrY=dv; //incrX (incrY): increment for E (NE) decision
    for (x=0; x<dx; x++)
    { yStore[x]=y;
      if (dv < 0) { dv += incrX; }
      else        { dv += incrY;  y += incr; } 
    }
  } 

#ifdef  DEBUG_LERP 
  if (yStore[0] != y0 || yStore[dx] != yn)
  { printf ("\rERROR IN set_iLerp!  (<CR> to continue) "); getchar(); }
#endif//DEBUG_LERP
  return OK;
}

/*************************************************************************/
  float lerp (float a, float b, float t)
/*************************************************************************/
/*Yields fraction t (t=0 ... 1) on the way from a to b (F.S.Hill, p. 160)*/
{ return a + (b - a) * t;
}

#ifdef  WITH_GLEVEL
/*************************************************************************/
  int ilerp (int first, int last, float t)
/*************************************************************************/
/*Yields the linearly partitioned integer closest to the fraction <t> 
  (t=0...1) of the way from value <first> to value <last>*/
{
#ifdef  MORE_LERP
#endif//MORE_LERP
}

/*************************************************************************/
  int gLevel (float intens, int nLevels)
/*************************************************************************/
/* Assign grey level (index 0...nLevels-1) to light intensity (0 ... 1) */
{ return (ilerp (0, nLevels-1, intens));
}
#endif//WITH_GLEVEL
